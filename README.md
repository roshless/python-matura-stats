# matura_stats
### simple cli for displaying matura exam statistics

```
usage: matura_stats [-h] [--men-only | --women-only]
                    {average,percent,highest-pass-rate,regressions,comparison}
                    ...

Simple tool for displaying stats about matura exams from 2010 to current year.

positional arguments:
  {average,percent,highest-pass-rate,regressions,comparison}
                        Commands to chose from.
    average             Returns average number of people that joined the exam
                        since 2010 to X year.
    percent             Percentage of success in specified region over the
                        years.
    highest-pass-rate   Show which region had highest pass rate in year X.
    regressions         Show regions which regressed with years.
    comparison          Compare pass rate each year between 2 regions.

optional arguments:
  -h, --help            show this help message and exit
  --men-only            Show only men.
  --women-only          Show only women.
```

As with most CLI programs, just pass --help or -h into subcommand to get more help.

# Run

First create virtual environment:

`virtualenv venv` 


And switch to it:

`source venv/bin/activate`


And install dependencies:

`pip install -r requirements.txt`


And run with:

`python main.py`
or use bash script on linux
`./matura-stats`


# Tests

Run with 

`pytest`

test_no_exceptions file tries every option you are allowed to call from CLI.