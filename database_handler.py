import os
import sqlite3


class DatabaseHandler(object):
    database_filename = 'matura_data.db'
    database_existed = False

    def __init__(self):
        self.database_existed = os.path.isfile(self.database_filename)
        self.conn = sqlite3.connect(self.database_filename)
        c = self.conn.cursor()
        c.execute('CREATE TABLE IF NOT EXISTS matura_passed(region text, sex text, year int, number_of_ppl int)')
        c.execute('CREATE TABLE IF NOT EXISTS matura_joined(region text, sex text, year int, number_of_ppl int)')
        self.conn.commit()

    def add_passed(self, input):
        c = self.conn.cursor()
        c.executemany('INSERT INTO matura_passed VALUES (?,?,?,?)', [input])
        self.conn.commit()

    def add_joined(self, input):
        c = self.conn.cursor()
        c.executemany('INSERT INTO matura_joined VALUES (?,?,?,?)', [input])
        self.conn.commit()

    def get_region_joined(self, region, year, sex):
        c = self.conn.cursor()
        if sex:
            c.execute('SELECT number_of_ppl FROM matura_joined WHERE region=? AND year=? AND sex=?',
                      [region, year, sex])
        else:
            c.execute('SELECT sum(number_of_ppl) FROM matura_joined WHERE region=? AND year=?', [region, year])

        ret_value = c.fetchone()[0]

        if not ret_value:
            raise ValueError('Database returned nothing')
        return ret_value


    def get_region_passed(self, region, year, sex):
        c = self.conn.cursor()
        if sex:
            c.execute('SELECT number_of_ppl FROM matura_passed WHERE region=? AND year=? AND sex=?',
                      [region, year, sex])
        else:
            c.execute('SELECT sum(number_of_ppl) FROM matura_passed WHERE region=? AND year=?', [region, year])

        ret_value = c.fetchone()[0]

        if not ret_value:
            raise ValueError('Database returned nothing')
        return ret_value
