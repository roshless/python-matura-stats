import pytest
from regions import regions

from data_manipulations import DataManipulation

both = DataManipulation(False, False)
females = DataManipulation(False, True)
males = DataManipulation(True, False)


def test_average():
    def test_average_common(sex):
        years = list(range(2010, 2018 + 1))
        for region in regions:
            for year in years:
                sex.average(region, year)
                # print("{} {} in {} average passed".format(sex.sex, region, year))

    test_average_common(both)
    test_average_common(females)
    test_average_common(males)


def test_percent():
    def test_percent_common(sex):
        for region in regions:
            sex.percent(region)
            # print("{} {} percent passed".format(sex.sex, region))

    test_percent_common(both)
    test_percent_common(females)
    test_percent_common(males)


def test_highest_pass_rate():
    def test_highest_pass_rate_common(sex):
        years = list(range(2010, 2018 + 1))

        for year in years:
            sex.highest_pass_rate(year)
            # print("{} highest pass rate in {} passed".format(sex.sex, year))

    test_highest_pass_rate_common(both)
    test_highest_pass_rate_common(females)
    test_highest_pass_rate_common(males)


def test_regressions():
    def test_regressions_common(sex):
        sex.regressions()
        # print("{} regressions passed".format(sex.sex))

    test_regressions_common(both)
    test_regressions_common(females)
    test_regressions_common(males)


def test_comparison():
    def test_comparison_common(sex):
        for region_one in regions:
            for region_two in regions:
                sex.comparison((region_one, region_two))
                # print("{} {} with {} comparison passed".format(sex.sex, region_one, region_two))

    test_comparison_common(both)
    test_comparison_common(females)
    test_comparison_common(males)
