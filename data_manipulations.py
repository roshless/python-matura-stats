from database_handler import DatabaseHandler
from regions import regions
from datetime import datetime


class DataManipulation(object):
    sex = None
    start_year = 2010
    current_year = datetime.now().year
    years = list(range(start_year, current_year))

    def __init__(self, male, female):
        if male:
            self.sex = 'mężczyźni'
        elif female:
            self.sex = 'kobiety'
        self.db_handler = DatabaseHandler()

    def average(self, region, year):
        def average(numbers):
            return int(sum(numbers) / len(numbers))

        data = list()
        years = list(range(self.start_year, year + 1))

        for y in years:
            data.extend([self.db_handler.get_region_joined(region, y, self.sex)])

        return average(data)

    def percent(self, region):
        data = list()

        for y in self.years:
            passed = self.db_handler.get_region_passed(region, y, self.sex)
            joined = self.db_handler.get_region_joined(region, y, self.sex)
            data.extend(["{} - {:.1%}".format(y, passed / joined)])

        return data

    def highest_pass_rate(self, year):
        highest_region = regions[0]
        highest_percentage = 0
        for region in regions:
            passed = self.db_handler.get_region_passed(region, year, self.sex)
            joined = self.db_handler.get_region_joined(region, year, self.sex)
            if passed / joined > highest_percentage:
                highest_percentage = passed / joined
                highest_region = region

        return "{} - {} {:.1%}".format(year, highest_region, highest_percentage)

    def regressions(self):
        regressions = list()

        for region in regions:
            last_year = self.db_handler.get_region_passed(region, self.start_year,
                                                          self.sex) / self.db_handler.get_region_joined(
                region, self.start_year, self.sex)

            for year in self.years:
                passed = self.db_handler.get_region_passed(region, year, self.sex)
                joined = self.db_handler.get_region_joined(region, year, self.sex)
                if passed / joined < last_year:
                    regressions.extend(
                        ["{}: {} -> {} - {} -> {}".format(region, year - 1, year, last_year, passed / joined)])
                last_year = passed / joined

        return regressions

    def comparison(self, two_regions):
        data_to_return = list()

        for year in self.years:
            region_one = self.db_handler.get_region_passed(two_regions[0], year, self.sex) \
                         / self.db_handler.get_region_joined(two_regions[0], year, self.sex)
            region_two = self.db_handler.get_region_passed(two_regions[1], year, self.sex) \
                         / self.db_handler.get_region_joined(two_regions[1], year, self.sex)

            if region_one > region_two:
                data_to_return.extend(["{} - {}".format(year, two_regions[0])])
            else:
                data_to_return.extend(["{} - {}".format(year, two_regions[1])])

        return data_to_return
